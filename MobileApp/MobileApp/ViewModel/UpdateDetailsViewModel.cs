﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DFCApi.Models;
using DFCApi.Models.Requests;
using GalaSoft.MvvmLight.Command;
using MobileApp.Helpers;
using MobileApp.Helpers.Api;

namespace MobileApp.ViewModel
{
    public class UpdateDetailsViewModel
    {
        public string LogoUrl { get; set; }

        public CustomerDetails CustomerDetails { get; set; }
        public Customer Customer { get; set; }

        public ICommand NavigationChangeuserDetailsCommand { get; set; }
        private readonly IMobileNavigationService _navigationService;

        public UpdateDetailsViewModel(IMobileNavigationService navigationService)
        {
            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            NavigationChangeuserDetailsCommand =
                new RelayCommand(() =>
                {
                    _navigationService.NavigateTo(Locator.CardDetailsPage);
                });
            {
                LogoUrl = ApiCustomers.GetBrandLogoUrl();

                Customer = ApiCustomers.GetCustomer();
                CustomerDetails = ApiCustomers.GetCustomerDetails();
            }
        }
    }
}
