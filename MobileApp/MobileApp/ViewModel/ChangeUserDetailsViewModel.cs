﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DFCApi.Models;
using DFCApi.Models.Requests;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using MobileApp.Helpers;
using MobileApp.Helpers.Api;
using MobileApp.Model;

namespace MobileApp.ViewModel
{
     public class ChangeUserDetailsViewModel
    {
        public CustomerDetails CustomerDetails { get; set; }
        public Customer Customer { get; set; }
        private readonly IMobileNavigationService _navigationService;

        public ChangeUserDetailsViewModel(IMobileNavigationService navigationService)
        {
            if (navigationService == null) throw new ArgumentNullException("navigationService");
        _navigationService = navigationService;

            NavigationChangeuserDetailsCommand =
                new RelayCommand(() =>
                {  
                    navigationService.CurrentPage.DisplayAlert("Success", "Details have been changed.", "OK");

                    ApiCustomers.AccountApiClient.UpdateAddress(new UpdateAddressRequest()
                    {
                        HouseNumber = CustomerDetails.HouseNumber,
                        PostCode = CustomerDetails.PostCode,
                        Street = CustomerDetails.Street,
                        Town = CustomerDetails.Town
                    });

                    ApiCustomers.AccountApiClient.UpdateCustomer(new UpdateCustomerRequest()
                    {
                        HomeTelephone = CustomerDetails.HomeTelephone,
                        MobileTelephone = CustomerDetails.MobileTelephone,
                        WorkTelephone = CustomerDetails.WorkTelephone
                    });

                    ApiCustomers.AccountApiClient.UpdateEmployment(new UpdateEmploymentRequest()
                    {
                        EmployerName = CustomerDetails.EmployerName,
                        NetMonthlyPay = CustomerDetails.NetMonthlyPay,
                    });

                    _navigationService.NavigateTo(Locator.MainMenuPage);
                });

            CustomerDetails = ApiCustomers.GetCustomerDetails();
        }

    public ICommand NavigationChangeuserDetailsCommand { get; set; }

    }
}
