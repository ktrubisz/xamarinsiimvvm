﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DFCApi.Models;
using GalaSoft.MvvmLight.Command;
using MobileApp.Helpers;
using MobileApp.Helpers.Api;
using Xamarin.Forms;

namespace MobileApp.ViewModel
{
    public class LoanOffersViewModel : INotifyPropertyChanged
    {
        private readonly IMobileNavigationService _navigationService;

        public ICommand NavigationCommand { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _isOfferChoosen;

        public bool IsOfferChoosen
        {
            get
            {
                return _isOfferChoosen;
            }
            set
            {
                _isOfferChoosen = value;
                OnPropertyChanged("IsOfferChoosen");
            }
        }
        public string LogoUrl { get; set; }
        private int _amount;
        public int Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                IsOfferChoosen = false;
                _amount = value;
                OnPropertyChanged("Amount");
                OnPropertyChanged("LoanOffers");
                LoanOffers = ApiCustomers.NewLoanApiClient.GetLoanOffers(Amount).Value.ToList();
            }
        }
        public List<LoanOffer> LoanOffers { get; set; }

        private LoanOffer _selectedLoanOffer;

        public LoanOffer SelectedLoanOffer
        {
            get
            {
                return _selectedLoanOffer;
            }
            set
            {
                if (value != null)
                {
                    _selectedLoanOffer = value;
                    IsOfferChoosen = true;
                }
                else
                {
                    _selectedLoanOffer = null;
                    IsOfferChoosen = false;
                }
            }
        }

        public LoanOffersViewModel(IMobileNavigationService navigationService)
        {
            //IsOfferChoosen = false;
            Amount = 300;

            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            LoanOffers = ApiCustomers.NewLoanApiClient.GetLoanOffers(Amount).Value.ToList();

            LogoUrl = ApiCustomers.GetBrandLogoUrl();

            NavigationCommand = new RelayCommand(() =>
            {
                ApiCustomers.ChoosenOffer = _selectedLoanOffer;
                _navigationService.NavigateTo(Locator.ContractPage);
            });
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this,
                new PropertyChangedEventArgs(propertyName));
        } 
    }
}
