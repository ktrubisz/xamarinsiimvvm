﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DFCApi.Models.Enums;
using GalaSoft.MvvmLight.Command;
using MobileApp.Helpers;
using MobileApp.Helpers.Api;
using MobileApp.Model;

namespace MobileApp.ViewModel
{
    public class CardDetailsViewModel
    {
        public string LogoUrl { get; set; }

        private readonly IMobileNavigationService _navigationService;
        public ICommand NavigationApproveLoanCommand { get; set; }

        public string[] DebitCardTypeStrings { get; set; }

        public CardDetailsViewModel(IMobileNavigationService navigationService)
        {
            //DebitCardTypeStrings = Enum.GetNames(typeof(DebitCardType));

            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            LogoUrl = ApiCustomers.GetBrandLogoUrl();

            NavigationApproveLoanCommand = new RelayCommand(
                () =>
                {

                    _navigationService.NavigateTo(Locator.LoanOffersPage);
                });
        }
    }
}
