﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DFCApi.Models;
using DFCApi.Models.Requests;
using GalaSoft.MvvmLight.Command;
using MobileApp.Helpers;
using MobileApp.Helpers.Api;

namespace MobileApp.ViewModel
{ 
    public class AccountSettingsViewModel
    {
        public string LogoUrl { get; set; }
        public MarketingPreferences MarketingPreferences { get; set; }

        private readonly IMobileNavigationService _navigationService;
        public ICommand NavigationChangeAccountSettingsCommand { get; set; }

        public AccountSettingsViewModel(IMobileNavigationService navigationService)
        {
            LogoUrl = ApiCustomers.GetBrandLogoUrl();

            MarketingPreferences = ApiCustomers.AccountApiClient.GetMarketing().Value;

            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            NavigationChangeAccountSettingsCommand =
                new RelayCommand(() =>
                {
                    navigationService.CurrentPage.DisplayAlert("Success", "Details have been changed.", "OK");
                    _navigationService.GoBack();
                });
        }
    }
}
