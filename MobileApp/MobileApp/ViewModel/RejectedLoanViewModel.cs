﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using MobileApp.Helpers;
using MobileApp.Helpers.Api;

namespace MobileApp.ViewModel
{
    public class RejectedLoanViewModel
    {
        public string LogoUrl { get; set; }
        public RejectedLoanViewModel(IMobileNavigationService navigationService)
        {

            LogoUrl = ApiCustomers.GetBrandLogoUrl();
        }
    }
}
