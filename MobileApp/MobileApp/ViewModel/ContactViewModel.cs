﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileApp.Helpers.Api;

namespace MobileApp.ViewModel
{
    public class ContactViewModel
    {
        public string LogoUrl { get; set; }

        public ContactViewModel()
        {
            LogoUrl = ApiCustomers.GetBrandLogoUrl();
        }
    }
}
