﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using Xamarin.Forms;
using MobileApp.Helpers.Api;
using MobileApp.Helpers;

namespace MobileApp.ViewModel
{
    public class LoginViewModel
    {
        Page _page;
        private readonly IMobileNavigationService _navigationService;

        public string Login { get; set; }
        public string Password { get; set; }
        public ICommand NavigationPasswordCommand { get; set; }
        public ICommand NavigationMainMenuCommand { get; set; }
        public ICommand NavigationNewCustomerCommand { get; set; }
        public DateTime Dob { get; set; }

        public LoginViewModel(IMobileNavigationService navigationService)
        {
            Dob = new DateTime(1980, 10, 10);
            Login = "test@test.te";
            Password = "12345";

            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;
            NavigationNewCustomerCommand =
                new RelayCommand(async () =>
                {
                    var result = await _navigationService.CurrentPage.DisplayAlert("New customer?", $"Go to www.themoneyshop.com", "OPEN PAGE", "CANCEL");

                    if (result)
                    {
                        Device.OpenUri(
                            new Uri("https://www.themoneyshop.com/"));
                    }

                });
            NavigationPasswordCommand =
                new RelayCommand(() => { _navigationService.NavigateTo(Locator.ForgottenPasswordPage); });
            NavigationMainMenuCommand = 
                new RelayCommand(() =>
                {
                    var result = ApiCustomers.Login(Login, Password);
                    if (result && Dob == new DateTime(1980, 10, 10))
                    {
                        _navigationService.NavigateTo(Locator.MainMenuPage);
                    }
                    else
                    {
                        _navigationService.CurrentPage.DisplayAlert("Login error", "It seems that a login error has occurred, please check your credentials.", "OK");
                    }
                });
        }
    }
}
