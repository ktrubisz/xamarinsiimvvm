﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using MobileApp.Helpers;
using Xamarin.Forms;

namespace MobileApp.ViewModel
{
     public class ForgottenPasswordViewModel
    {
        private readonly IMobileNavigationService _navigationService;

        public ForgottenPasswordViewModel(IMobileNavigationService navigationService)
        {
            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            NavigationCommand =
                new RelayCommand(() =>
                {
                    _navigationService.CurrentPage.DisplayAlert("Success", "New password has been send to you.", "OK");
                    _navigationService.GoBack();
                });
        }

        public ICommand NavigationCommand { get; set; }
        public string Parameter { get; set; }
    }
}
