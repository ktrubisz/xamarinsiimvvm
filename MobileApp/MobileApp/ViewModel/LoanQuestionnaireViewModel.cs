﻿using DFCApi.Models.Enums;
using MobileApp.Helpers.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MobileApp.Helpers;
using Xamarin.Forms;

namespace MobileApp.ViewModel
{
    public class LoanQuestionnaireViewModel
    {
        public string LogoUrl { get; }

        public string[] SourceOFIncomeStrings { get; set; }

        public ICommand NavigatioConfirmCommand { get; set; }
        private readonly IMobileNavigationService _navigationService;

        public LoanQuestionnaireViewModel(IMobileNavigationService navigationService)
        {
            if (navigationService == null) throw new ArgumentNullException("navigationService");
        _navigationService = navigationService;

            NavigatioConfirmCommand =
                new RelayCommand(() => { _navigationService.NavigateTo(Locator.UpdateDetailsPage); });
            
            LogoUrl = ApiCustomers.GetBrandLogoUrl();

            SourceOFIncomeStrings = Enum.GetNames(typeof(SourceOfIncome));
        }
    }
}
