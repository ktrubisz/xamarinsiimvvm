﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileApp.Helpers.Api;
using MobileApp.Helpers;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;

namespace MobileApp.ViewModel
{
    public class ChangePasswordViewModel
    {
        private readonly IMobileNavigationService _navigationService;

        public string LogoUrl { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public ICommand ChangePasswordCommand { get; set; }

        public ChangePasswordViewModel(IMobileNavigationService navigationService)
        {
            LogoUrl = ApiCustomers.GetBrandLogoUrl();

            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;
            ChangePasswordCommand = new RelayCommand(() => 
                {
                    if (NewPassword == ConfirmPassword)
                    {
                        var result = ApiCustomers.ChangePassword(CurrentPassword, NewPassword);
                        if (result)
                        {   
                            _navigationService.CurrentPage.DisplayAlert("Success", "Password has been changed successfully.", "OK");
                            _navigationService.GoBack();
                        }
                        else
                        {
                            _navigationService.CurrentPage.DisplayAlert("Error", "Password change could not proceed, please make sure that all the data is correct.", "OK");
                        }
                    }
                    else
                    {
                        _navigationService.CurrentPage.DisplayAlert("Error", "Password change could not proceed, please make sure that all the data is correct.", "OK");
                    }
                });
        }
    }
}
