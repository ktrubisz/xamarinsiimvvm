﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using DFCApi.Models;
using DFCApi.Models.Enums;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using MobileApp.Helpers.Api;

namespace MobileApp.ViewModel
{
    public class MainMenuViewModel
    {
        private readonly INavigationService _navigationService;
        public Customer Customer { get; set; }
        public string LogoUrl { get; set; }
        public MainMenuViewModel(INavigationService navigationService)
        {
            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            Customer = ApiCustomers.GetCustomer();

            LogoUrl = ApiCustomers.GetBrandLogoUrl();

            NavigationChangePasswordCommand = new RelayCommand(
                () => { _navigationService.NavigateTo(Locator.ChangePasswordPage); });
            NavigationAccountSettingsCommand = new RelayCommand(
                () => { _navigationService.NavigateTo(Locator.AccountSettingsPage); });
            NavigationLoanDetailsCommand = new RelayCommand(
                () => { _navigationService.NavigateTo(Locator.LoanDetailsPage); });
            NavigationContactCommand = new RelayCommand(
                () => { _navigationService.NavigateTo(Locator.ContactPage); });
            NavigationUserDetailsCommand = new RelayCommand(
                () => { _navigationService.NavigateTo(Locator.UserDetailsPage); });
            NavigationCardDetailsCommand = new RelayCommand(
                () => { _navigationService.NavigateTo(Locator.CardDetailsPage); });
            NavigationApplyForLoanCommand = new RelayCommand(
                () => { _navigationService.NavigateTo(Locator.ApplyForLoanPage); });
            NavigationLogoutCommand = new RelayCommand(
                () =>
                {
                    //ApiCustomers.AccountApiClient.ApiLogout();
                    _navigationService.NavigateTo(Locator.LoginPage);
                });
        }

        public ICommand NavigationLogoutCommand { get; set; }
        public ICommand NavigationApplyForLoanCommand { get; set; }
        public ICommand NavigationChangePasswordCommand { get; set; }
        public ICommand NavigationAccountSettingsCommand { get; set; }
        public ICommand NavigationLoanDetailsCommand { get; set; }
        public ICommand NavigationContactCommand { get; set; }
        public ICommand NavigationUserDetailsCommand { get; set; }
        public ICommand NavigationCardDetailsCommand { get; set; }
    }
}
