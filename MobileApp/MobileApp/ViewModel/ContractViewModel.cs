﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MobileApp.Helpers;
using MobileApp.Helpers.Api;
using Xamarin.Forms;

namespace MobileApp.ViewModel
{
    public class ContractViewModel
    {
        public string LogoUrl { get; set; }
        private readonly IMobileNavigationService _navigationService;
        public ICommand NavigationNewCustomerCommand { get; set; }
        public DateTime Dob { get; set; }

        public string AmountString { get; set; }

        public ContractViewModel(IMobileNavigationService navigationService)
        {
            LogoUrl = ApiCustomers.GetBrandLogoUrl();

            var offer = ApiCustomers.ChoosenOffer;

            AmountString = $"Amount:{offer.Amount}, Installments:{offer.Installments}, Repay:{offer.Repay}";

            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;
            NavigationNewCustomerCommand =
                new RelayCommand(() =>
                {
                    _navigationService.CurrentPage.DisplayAlert("Confirm", "Done",
                        "OK");

                    _navigationService.NavigateTo(Locator.LoanDetailsPage);

                });
        }
    }
}
