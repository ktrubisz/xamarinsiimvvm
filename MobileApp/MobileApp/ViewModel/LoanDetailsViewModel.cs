﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DFCApi.Models;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using MobileApp.Helpers;
using MobileApp.Helpers.Api;
using MobileApp.View;
using Xamarin.Forms;

namespace MobileApp.ViewModel
{
    public class LoanDetailsViewModel
    {
        public Loan Loan { get; set; }

        public string LogoUrl { get; set; }
        public LoanDetailsViewModel(IMobileNavigationService navigationService)
        {

            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            Loan = ApiCustomers.FinancialApiClient.GetActiveLoan().Value;

            LogoUrl = ApiCustomers.GetBrandLogoUrl();

            NavigationCommand =
                new RelayCommand(() =>
                {
                    _navigationService.NavigateTo(Locator.MainMenuPage);
                });
        }

        private readonly IMobileNavigationService _navigationService;

        public ICommand NavigationCommand { get; set; }
    }
}
