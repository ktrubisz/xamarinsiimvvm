﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DFCApi.Models;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using MobileApp.Helpers;
using MobileApp.Helpers.Api;

namespace MobileApp.ViewModel
{
    public class UserDetailsViewModel
    {
        private readonly IMobileNavigationService _navigationService;

        public Customer Customer { get; set; }
        public CustomerDetails CustomerDetails { get;}
        public string LogoUrl { get; set; }

        public string FullTimeWithEmployer => $"{CustomerDetails.YearsAtEmployer}years, {CustomerDetails.MonthAtEmployer}months";
        public string FullAddress => $"{CustomerDetails.Town}, {CustomerDetails.Street} {CustomerDetails.HouseNumber}";

        public UserDetailsViewModel(IMobileNavigationService navigationService)
        {
            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;
            NavigationChangeUserDetailsCommand =
                new RelayCommand(() => { _navigationService.NavigateTo(Locator.ChangeUserDetailsPage); });

            NavigationPasswordCommand =
                new RelayCommand(() => { _navigationService.NavigateTo(Locator.ChangeUserDetailsPage); });

            Customer = ApiCustomers.GetCustomer();
            LogoUrl = ApiCustomers.GetBrandLogoUrl();
            CustomerDetails = ApiCustomers.GetCustomerDetails();
        }

        public ICommand NavigationChangeUserDetailsCommand { get; set; }

        public ICommand NavigationPasswordCommand { get; set; }

    }
}
