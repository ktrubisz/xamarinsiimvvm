﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DFCApi.Models.Enums;
using DFCApi.Models.Requests;
using GalaSoft.MvvmLight.Command;
using MobileApp.Helpers;
using MobileApp.Helpers.Api;
using MobileApp.Model;

namespace MobileApp.ViewModel
{
    public class ApplyForLoanViewModel
    {
        public string LogoUrl { get; }

        public bool ConfirmFinancialInformation { get; set; }
        public bool ConfirmOutgoings { get; set; }

        public RequestLoanModel RequestLoanModel  { get; set; }

        private readonly IMobileNavigationService _navigationService;
        public ICommand NavigationApproveLoanCommand { get; set; }

        public ApplyForLoanViewModel(IMobileNavigationService navigationService)
        {
            if (navigationService == null) throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            RequestLoanModel = new RequestLoanModel();

            NavigationApproveLoanCommand = new RelayCommand(
                () =>
                {
                    var req2 = new NewLoanRequest()
                    {
                        Amount = RequestLoanModel.Amount,
                        Installments = RequestLoanModel.Installments,
                        NetMonthlyPay = RequestLoanModel.NetMonthlyPay,
                        CreditExpenditure = RequestLoanModel.CreditExpenditure,
                        MortgageExpenditure = RequestLoanModel.MortgageExpenditure,
                        UtilitiesExpenditure = RequestLoanModel.UtilitiesExpenditure,
                        TransportExpenditure = RequestLoanModel.TransportExpenditure,
                        FoodExpenditure = RequestLoanModel.FoodExpenditure,
                        OtherExpenditure = RequestLoanModel.OtherExpenditure,
                        ConfirmAbilityToRepay = RequestLoanModel.ConfirmAbilityToRepay,

                        FutureIncome = RequestLoanModel.FutureIncome,
                        IntentToPay = RequestLoanModel.IntentToPay,
                        IsBankrupt = RequestLoanModel.IsBankrupt,
                        IsInDebtManagement = RequestLoanModel.IsInDebtManagement,
                        IsInFinancialDifficulty = RequestLoanModel.IsInFinancialDifficulty,
                        ConfirmHighCost = RequestLoanModel.ConfirmHighCost,
                        Purpose = RequestLoanModel.Purpose,
                        SourceOfIncome = RequestLoanModel.SourceOfIncome
                    };


                    var apply = ApiCustomers.NewLoanApiClient.ApplyForLoan(req2);

                    _navigationService.NavigateTo(req2.OtherExpenditure == 0 ? Locator.LoanQuestionnairePage : Locator.RejectedLoanPage);
                    //_navigationService.NavigateTo(apply.IsSuccess() ? Locator.LoanOffersPage : Locator.RejectedLoanPage);
                });

            LogoUrl = ApiCustomers.GetBrandLogoUrl();
  
        }
    }
}
