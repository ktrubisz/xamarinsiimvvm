﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using MobileApp.ViewModel;
using MobileApp.View;

namespace MobileApp
{
    public class Locator
    {
        /// <summary>
        /// Register all the used ViewModels, Services et. al. witht the IoC Container
        /// </summary>
        public Locator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            // ViewModels
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<ForgottenPasswordViewModel>();
            SimpleIoc.Default.Register<MainMenuViewModel>();
            SimpleIoc.Default.Register<ChangePasswordViewModel>();
            SimpleIoc.Default.Register<AccountSettingsViewModel>();
            SimpleIoc.Default.Register<LoanDetailsViewModel>();
            SimpleIoc.Default.Register<ContactViewModel>();
            SimpleIoc.Default.Register<UserDetailsViewModel>();
            SimpleIoc.Default.Register<CardDetailsViewModel>();
            SimpleIoc.Default.Register<ChangeUserDetailsViewModel>();
            SimpleIoc.Default.Register<ApplyForLoanViewModel>();
            SimpleIoc.Default.Register<LoanOffersViewModel>();
            SimpleIoc.Default.Register<RejectedLoanViewModel>();
            SimpleIoc.Default.Register<LoanQuestionnaireViewModel>();
            SimpleIoc.Default.Register<UpdateDetailsViewModel>();
            SimpleIoc.Default.Register<ContractViewModel>();

            // Views
            SimpleIoc.Default.Register<LoginPage>();
            SimpleIoc.Default.Register<ForgottenPasswordPage>();
            SimpleIoc.Default.Register<MainMenuPage>();
            SimpleIoc.Default.Register<ChangePasswordPage>();
            SimpleIoc.Default.Register<AccountSettingsPage>();
            SimpleIoc.Default.Register<LoanDetailsPage>();
            SimpleIoc.Default.Register<ContactPage>();
            SimpleIoc.Default.Register<UserDetailsPage>();
            SimpleIoc.Default.Register<CardDetailsPage>();
            SimpleIoc.Default.Register<ChangeUserDetailsPage>();
            SimpleIoc.Default.Register<ApplyForLoanPage>();
            SimpleIoc.Default.Register<LoanOffersPage>();
            SimpleIoc.Default.Register<RejectedLoanPage>();
            SimpleIoc.Default.Register<LoanQuestionnairePage>();
            SimpleIoc.Default.Register<UpdateDetailsPage>();
            SimpleIoc.Default.Register<ContractPage>();
        }


        public const string LoginPage = "LoginPage";
        public const string ForgottenPasswordPage = "ForgottenPasswordPage";
        public const string MainMenuPage = "MainMenuPage";
        public const string ChangePasswordPage = "ChangePasswordPage";
        public const string AccountSettingsPage = "AccountSettingsPage";
        public const string LoanDetailsPage = "LoanDetailsPage";
        public const string ContactPage = "ContactPage";
        public const string UserDetailsPage = "UserDetailsPage";
        public const string CardDetailsPage = "CardDetailsPage";
        public const string ChangeUserDetailsPage = "ChangeUserDetailsPage";
        public const string ApplyForLoanPage = "ApplyForLoanPage";
        public const string ApproveLoanPage = "ApproveLoanPage";
        public const string LoanOffersPage = "LoanOffersPage";
        public const string RejectedLoanPage = "RejectedLoanPage";
        public const string LoanQuestionnairePage = "LoanQuestionnairePage";
        public const string UpdateDetailsPage = "UpdateDetailsPage";
        public const string ContractPage = "ContractPage";

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public LoginViewModel Login => ServiceLocator.Current.GetInstance<LoginViewModel>();
        

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public ForgottenPasswordViewModel ForgottenPassword => ServiceLocator.Current.GetInstance<ForgottenPasswordViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public MainMenuViewModel MainMenu => ServiceLocator.Current.GetInstance<MainMenuViewModel>(); 

        

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public ChangePasswordViewModel ChangePassword =>  ServiceLocator.Current.GetInstance<ChangePasswordViewModel>(); 
        

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public LoanDetailsViewModel LoanDetails => ServiceLocator.Current.GetInstance<LoanDetailsViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public ContactViewModel Contact => ServiceLocator.Current.GetInstance<ContactViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public UserDetailsViewModel UserDetails => ServiceLocator.Current.GetInstance<UserDetailsViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public AccountSettingsViewModel AccountSettings => ServiceLocator.Current.GetInstance<AccountSettingsViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public CardDetailsViewModel CardDetails => ServiceLocator.Current.GetInstance<CardDetailsViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public ChangeUserDetailsViewModel ChangeUserDetails => ServiceLocator.Current.GetInstance<ChangeUserDetailsViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public ApplyForLoanViewModel ApplyForLoan => ServiceLocator.Current.GetInstance<ApplyForLoanViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public LoanOffersViewModel LoanOffers => ServiceLocator.Current.GetInstance<LoanOffersViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public RejectedLoanViewModel RejectedLoan => ServiceLocator.Current.GetInstance<RejectedLoanViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public LoanQuestionnaireViewModel LoanQuestionnaire => ServiceLocator.Current.GetInstance<LoanQuestionnaireViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public UpdateDetailsViewModel UpdateDetails => ServiceLocator.Current.GetInstance<UpdateDetailsViewModel>();

        /// <summary>
        /// Gets the Second property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
             "CA1822:MarkMembersAsStatic",
             Justification = "This non-static member is needed for data binding purposes.")]
        public ContractViewModel Contract => ServiceLocator.Current.GetInstance<ContractViewModel>();
    }
}
