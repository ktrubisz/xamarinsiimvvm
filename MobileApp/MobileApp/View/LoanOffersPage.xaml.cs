﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MobileApp.View
{
    public partial class LoanOffersPage : ContentPage
    {
        public LoanOffersPage()
        {
            InitializeComponent();
            BindingContext = App.Locator.LoanOffers;

            NavigationPage.SetHasBackButton(this, false);
        }
    }
}
