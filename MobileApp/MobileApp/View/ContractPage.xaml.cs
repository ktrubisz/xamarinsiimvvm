﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MobileApp.View
{
    public partial class ContractPage : ContentPage
    {
        public ContractPage()
        {
            InitializeComponent();
            BindingContext = App.Locator.Contract;
        }
    }
}
