﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MobileApp.View
{
    public partial class LoanQuestionnairePage : ContentPage
    {
        public LoanQuestionnairePage()
        {
            InitializeComponent();
            BindingContext = App.Locator.LoanQuestionnaire;
        }
    }
}
