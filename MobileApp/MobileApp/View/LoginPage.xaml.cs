﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MobileApp.View
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();

            //App.Locator.Login.Page = this;
            BindingContext = App.Locator.Login;

            NavigationPage.SetHasBackButton(this, false);
        }
    }
}
