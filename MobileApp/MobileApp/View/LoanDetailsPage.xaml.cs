﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MobileApp.View
{
    public partial class LoanDetailsPage : ContentPage
    {
        public LoanDetailsPage()
        {
            InitializeComponent();

            BindingContext = App.Locator.LoanDetails;
        }
    }
}
