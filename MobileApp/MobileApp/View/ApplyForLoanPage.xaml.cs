﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MobileApp.View
{
    public partial class ApplyForLoanPage : ContentPage
    {
        public ApplyForLoanPage()
        {
            InitializeComponent();

            BindingContext = App.Locator.ApplyForLoan;
        }
    }
}
