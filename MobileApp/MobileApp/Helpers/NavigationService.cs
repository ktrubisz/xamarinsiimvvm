﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using MobileApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

public class NavigationService : IMobileNavigationService
{
    private readonly Dictionary<string, Type> _pagesByKey = new Dictionary<string, Type>();
    private NavigationPage _navigation;

    public string CurrentPageKey
    {
        get
        {
            lock (_pagesByKey)
            {
                if (_navigation.CurrentPage == null)
                {
                    return null;
                }

                var pageType = _navigation.CurrentPage.GetType();

                return _pagesByKey.ContainsValue(pageType)
                    ? _pagesByKey.First(p => p.Value == pageType).Key
                    : null;
            }
        }
    }

    public Page CurrentPage
    {
        get
        {
            return _navigation.CurrentPage;
        }
    }

    public void GoBack()
    {
        _navigation.PopAsync();
    }
    
    public void NavigateTo(string pageKey, object parameter)
    {
        if (parameter != null)
        {
            throw new NotImplementedException("Please use NavigateTo(string pageKey)");
        }
        else
        {
            NavigateTo(pageKey);
        }
    }

    public void NavigateTo(string pageKey)
    {
        lock (_pagesByKey)
        {
            if (_pagesByKey.ContainsKey(pageKey))
            {
                var type = _pagesByKey[pageKey];

                //try
                //{ 
                    var page = SimpleIoc.Default.GetInstance(type) as Page;
                    if (page == null)
                    {
                        throw new NotImplementedException("TODO: throw better exception");
                    }

                    _navigation.PushAsync(page);
                //}
                //catch (Exception exc)
                //{
                //    var x = 1;
                //}
            }
            else
            {
                throw new ArgumentException(
                    string.Format(
                        "No such page: {0}. Did you forget to call NavigationService.Configure?",
                        pageKey),
                    "pageKey");
            }
        }
    }

    public void Configure(string pageKey, Type pageType)
    {
        lock (_pagesByKey)
        {
            if (_pagesByKey.ContainsKey(pageKey))
            {
                _pagesByKey[pageKey] = pageType;
            }
            else
            {
                _pagesByKey.Add(pageKey, pageType);
            }
        }
    }

    public void Initialize(NavigationPage navigation)
    {
        _navigation = navigation;
    }
}
