﻿using GalaSoft.MvvmLight.Views;
using Xamarin.Forms;

namespace MobileApp.Helpers
{
    public interface IMobileNavigationService : INavigationService
    {
        Page CurrentPage { get; }
    }
}
