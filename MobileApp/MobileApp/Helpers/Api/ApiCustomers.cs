﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DFCApi.Models;
using DFCApi.Models.Enums;
using DFCApi.WebApiClient.Clients;

namespace MobileApp.Helpers.Api
{
   static class ApiCustomers
   {
       public static LoanOffer ChoosenOffer;

        public static AccountApiClient AccountApiClient;
        public static FinancialApiClient FinancialApiClient;
        public static NewLoanApiClient NewLoanApiClient;

        private static Customer _customer;

        public static TokenStorage TokenStorage;
        public static ApiConfig ApiConfig;

        public static Customer GetCustomer() => _customer;
        
        public static CustomerDetails GetCustomerDetails() => AccountApiClient.GetCustomerDetails().Value;

        public static MarketingPreferences GetMarketingPreferences() => AccountApiClient.GetMarketing().Value; 

        public static void ConnectToApi()
        {
            TokenStorage = new TokenStorage();
            ApiConfig = new ApiConfig();

            AccountApiClient = new AccountApiClient(ApiConfig, TokenStorage);
            FinancialApiClient = new FinancialApiClient(ApiConfig, TokenStorage);
            NewLoanApiClient = new NewLoanApiClient(ApiConfig, TokenStorage);
        }

        public static string GetBrandLogoUrl()
        {
            if(AccountApiClient.GetLoggedUser().Value.Brand == Brand.TheMoneyShop) return "https://www.themoneyshop.com/media/1123/logo-header.png";
            else return "https://www.paydayexpress.co.uk/images/pex_logo.gif";

        }

        public static bool ChangePassword(string currentPassword, string newPassword)
        {
            try
            {
                AccountApiClient.ChangePassword(currentPassword, newPassword);
                return true;
            }
            catch(Exception exc)
            {
                return false;
            }
        }

        public static bool Login(string email, string password)
        {
                _customer = AccountApiClient.ApiLogin(email, password).Value;
            return true;
        }
    }
}
