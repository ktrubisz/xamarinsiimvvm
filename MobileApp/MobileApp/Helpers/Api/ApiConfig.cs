﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DFCApi.WebApiClient.Interfaces;

namespace MobileApp.Helpers.Api
{
    internal class ApiConfig : IApiConfiguration
    {
        public string ApiUrl => "http://xamarinsiiwebapi.azurewebsites.net/";
    }
}
