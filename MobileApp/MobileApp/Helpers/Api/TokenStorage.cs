﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DFCApi.WebApiClient.Interfaces;

namespace MobileApp.Helpers.Api
{
    public class TokenStorage : ITokenStorage
    {
        private static string _tokenType;
        private static string _tokenValue;

        public void StoreToken(string tokenType, string tokenValue)
        {
            _tokenType = tokenType;
            _tokenValue = tokenValue;
        }

        public string RetreiveTokenType()
        {
            return _tokenType;
        }

        public string RetreiveTokenValue()
        {
            return _tokenValue;
        }

        public void ClearToken()
        {
            _tokenType = null;
            _tokenValue = null;
        }
    }
}
