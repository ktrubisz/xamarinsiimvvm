﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DFCApi.Models.Enums;

namespace MobileApp.Model
{
    public class RequestLoanModel
    {
        //Page one
        public int Amount { get; set; }
        public int Installments { get; set; }
        public decimal NetMonthlyPay { get; set; }
        public decimal CreditExpenditure { get; set; }
        public decimal MortgageExpenditure { get; set; }
        public decimal UtilitiesExpenditure { get; set; }
        public decimal TransportExpenditure { get; set; }
        public decimal FoodExpenditure { get; set; }
        public decimal OtherExpenditure { get; set; }
        public bool ConfirmAbilityToRepay { get; set; }

        //Page two

        public LoanPurpose Purpose { get; set; }

        public bool IsInDebtManagement { get; set; }

        public bool IsBankrupt { get; set; }

        public bool IsInFinancialDifficulty { get; set; }

        public FutureIncome FutureIncome { get; set; }

        public bool IntentToPay { get; set; }

        public SourceOfIncome SourceOfIncome { get; set; }

        public bool ConfirmHighCost { get; set; }
    }
}
