﻿using MobileApp.Helpers.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using MobileApp.Helpers;
using MobileApp.View;
using Xamarin.Forms;

namespace MobileApp
{
    public partial class App : Application
    {
 
        private static Locator _locator;
        public static Locator Locator => _locator ?? (_locator = new Locator());

        public App()
        {
            InitializeComponent();

            ApiCustomers.ConnectToApi();
            //ApiCustomers.Login("test@test2.te", "12345");

            var nav = new NavigationService();
            nav.Configure(Locator.LoginPage, typeof(LoginPage));
            nav.Configure(Locator.ForgottenPasswordPage, typeof(ForgottenPasswordPage));
            nav.Configure(Locator.MainMenuPage, typeof(MainMenuPage));
            nav.Configure(Locator.ChangePasswordPage, typeof(ChangePasswordPage));
            nav.Configure(Locator.AccountSettingsPage, typeof(AccountSettingsPage));
            nav.Configure(Locator.LoanDetailsPage, typeof(LoanDetailsPage));
            nav.Configure(Locator.ContactPage, typeof(ContactPage));
            nav.Configure(Locator.UserDetailsPage, typeof(UserDetailsPage));
            nav.Configure(Locator.CardDetailsPage, typeof(CardDetailsPage));
            nav.Configure(Locator.ChangeUserDetailsPage, typeof(ChangeUserDetailsPage));
            nav.Configure(Locator.ApplyForLoanPage, typeof(ApplyForLoanPage));
            nav.Configure(Locator.LoanOffersPage, typeof(LoanOffersPage));
            nav.Configure(Locator.RejectedLoanPage, typeof(RejectedLoanPage));
            nav.Configure(Locator.LoanQuestionnairePage, typeof(LoanQuestionnairePage));
            nav.Configure(Locator.UpdateDetailsPage, typeof(UpdateDetailsPage));
            nav.Configure(Locator.ContractPage, typeof(ContractPage));

            SimpleIoc.Default.Register<INavigationService>(() => nav);
            SimpleIoc.Default.Register<IMobileNavigationService>(() => nav);

            var loginPage = new NavigationPage(new LoginPage());

            nav.Initialize(loginPage);

            //SimpleIoc.Default.Register<INavigationService>(() => nav);

            MainPage = loginPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
